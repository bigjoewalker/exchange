CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_9ec886924bcd97ae6f14220017` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `product` (
  `OMSId` int NOT NULL,
  `ProductId` int NOT NULL,
  `Product` varchar(50) NOT NULL,
  `ProductFullName` varchar(50) NOT NULL,
  `MasterDataUniqueProductSymbol` varchar(50) NOT NULL,
  `ProductType` varchar(50) NOT NULL,
  `DecimalPlaces` int NOT NULL,
  `TickSize` float NOT NULL,
  `NoFees` tinyint NOT NULL,
  `IsDisabled` tinyint NOT NULL,
  `MarginEnabled` tinyint NOT NULL,
  PRIMARY KEY (`ProductId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `instrument` (
  `OMSId` int NOT NULL,
  `InstrumentId` int NOT NULL,
  `Symbol` varchar(50) NOT NULL,
  `Product1` int NOT NULL,
  `Product1Symbol` varchar(50) NOT NULL,
  `Product2` int NOT NULL,
  `Product2Symbol` varchar(50) NOT NULL,
  `InstrumentType` varchar(50) NOT NULL,
  `VenueInstrumentId` int NOT NULL,
  `VenueId` int NOT NULL,
  `SortIndex` int NOT NULL,
  `SessionStatus` varchar(50) NOT NULL,
  `PreviousSessionStatus` varchar(50) NOT NULL,
  `SessionStatusDateTime` varchar(50) NOT NULL,
  `SelfTradePrevention` tinyint NOT NULL,
  `QuantityIncrement` float NOT NULL,
  `PriceIncrement` float NOT NULL,
  `MinimumQuantity` float NOT NULL,
  `MinimumPrice` float NOT NULL,
  `VenueSymbol` varchar(50) NOT NULL,
  `IsDisable` tinyint NOT NULL,
  `MasterDataId` int NOT NULL,
  `PriceCollarThreshold` float NOT NULL,
  `PriceCollarPercent` float NOT NULL,
  `PriceCollarEnabled` tinyint NOT NULL,
  `PriceFloorLimit` float NOT NULL,
  `PriceFloorLimitEnabled` tinyint NOT NULL,
  `PriceCeilingLimit` float NOT NULL,
  `PriceCeilingLimitEnabled` tinyint NOT NULL,
  `CreateWithMarketRunning` tinyint NOT NULL,
  `AllowOnlyMarketMakerCounterParty` tinyint NOT NULL,
  `PriceCollarIndexDifference` float NOT NULL,
  `PriceCollarConvertToOtcEnabled` tinyint NOT NULL,
  `PriceCollarConvertToOtcClientUserId` int NOT NULL,
  `PriceCollarConvertToOtcAccountId` int NOT NULL,
  `PriceCollarConvertToOtcThreshold` float NOT NULL,
  `OtcConvertSizeThreshold` float NOT NULL,
  `OtcConvertSizeEnabled` tinyint NOT NULL,
  `OtcTradesPublic` tinyint NOT NULL,
  `PriceTier` int NOT NULL,
  PRIMARY KEY (`InstrumentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;;

insert into user (user, pass, rols) values ('admin', '$2b$10$WC89dt.AjdBPjNYBoF.XLOQdB/mLmt9mVtJVM6cPYbU26QgxHodyi', 'admin');
