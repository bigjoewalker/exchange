import bcrypt from 'bcrypt'
import sinon from 'sinon'
import jwt from 'jsonwebtoken'
import * as service from './auth.service'

afterEach(() => {
    jest.clearAllMocks()
    sinon.restore()
})

describe('auth.service', () => {
    const mockUser = {
        id: 1,
        user: 'admin',
        role: 'admin',
    }

    describe('getToken', () => {
        test('it should return token', async () => {
            sinon.stub(jwt, 'sign').resolves({token: '1234'})
            const {token} = await service.getToken(mockUser)
            expect(token).toBe('1234')
        })
    })

    describe('verifyToken', () => {
        test('it should return true if token valid', async () => {
            sinon.stub(jwt, 'verify').resolves(mockUser)
            const user = await service.verifyToken('example token')
            expect(user).toEqual(mockUser)
        })

        test('it should return false if token invalid', async () => {
            sinon.stub(jwt, 'verify').rejects('token is invalid')
            try {
                await service.verifyToken('example token')
            } catch (e) {
                expect(e).not.toBeUndefined()
                expect(e.name).toBe('token is invalid')
            }
        })
    })

    describe('comparePassword', () => {
        test('it should return true if password is valid', async () => {
            const hash = await bcrypt.hash('root', 10)
            const status = await service.comparePassword('root', hash)
            expect(status).toBe(true)
        })
    })
})