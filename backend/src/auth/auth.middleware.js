import * as service from './auth.service'

export const authValidateToken = async (req, res, next) => {
    try {
        const header = req.get('Authorization')
        const token = header.split(' ')[1]

        const user = await service.verifyToken(token)
        if(!user) {
            return res.sendStatus(403)
        }
        res.locals.user = user
        next()
    } catch (e) {
        console.log(`token is invalide: ${e}`)
        return res.sendStatus(401)
    }
}