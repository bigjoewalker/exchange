import sinon from 'sinon'
import * as middleware from './auth.middleware'
import * as service from './auth.service'

beforeAll(() => {
    jest.spyOn(console, 'log').mockImplementation(() => {
    })
})

afterEach(() => {
    jest.clearAllMocks()
    sinon.restore()
})

describe('auth.middleware', () => {
    const mockUser = {
        id: 1,
        user: 'admin',
        role: 'admin',
    }
    const req = {
        get() {
            return 'Bearer token'
        }
    }
    const res = {
        locals: {},
        sendStatus: jest.fn(),
        send: jest.fn(),
    }
    const next = jest.fn()

    describe('authValidateToken', () => {
        test('it should return user info if token valid', async () => {
            sinon.stub(service, 'verifyToken').resolves({...mockUser})
            await middleware.authValidateToken(req, res, next)
            expect(res.locals.user).toEqual(mockUser)
            expect(next).toHaveBeenCalled()
        })

        test('it should return http status code 403 if user null', async () => {
            sinon.stub(service, 'verifyToken').resolves(null)
            res.sendStatus.mockReturnValue(403)
            await middleware.authValidateToken(req, res, next)
            expect(res.sendStatus).toHaveBeenCalledWith(403)
        })

        test('it should return http status code 401 if verify token fail', async () => {
            sinon.stub(service, 'verifyToken').rejects({error: 'verify token fail'})
            res.sendStatus.mockReturnValue(401)
            await middleware.authValidateToken(req, res, next)
            expect(res.sendStatus).toHaveBeenCalledWith(401)
        })
    })
})