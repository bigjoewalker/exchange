import * as service from './auth.service'
import * as repository from '../user/user.repository'
import * as db from '../database/database.connection'

export const login = async (req, res) => {
    try {
        const {user, pass} = req.body
        const pool = await db.getPool()
        const userInfo = await repository.findOne(pool, {user})
        const isValidPass = await service.comparePassword(pass, userInfo.pass)
        if (!isValidPass) {
            return res.sendStatus(403)
        }

        delete userInfo.pass
        const token = await service.getToken(userInfo)
        return res.send({token})
    } catch (e) {
        console.log(`auth fail: ${e}`)
        return res.sendStatus(500)
    }
}
