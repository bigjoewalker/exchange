import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const secretKey = process.env.SECRET_KEY || 'secret'

export const getToken = (user) => (
    jwt
        .sign(
            {...user},
            `${secretKey}`,
            {
                algorithm: 'HS512',
                expiresIn: '1d',
            })
)

export const verifyToken = (token) => jwt.verify(token, secretKey)

export const comparePassword = async (password, hash) => await bcrypt.compare(password, hash)
