import sinon from 'sinon'
import * as authHandler from './auth.handler'
import * as repository from '../user/user.repository'
import * as service from './auth.service'

beforeAll(() => {
    jest.spyOn(console, 'log').mockImplementation(() => {
    })
})

afterEach(() => {
    jest.clearAllMocks()
    sinon.restore()
})

describe('auth.handler', () => {
    const req = {
        body: {
            user: 'amdin',
            pass: 'admin',
        }
    }
    const res = {
        locals: {},
        sendStatus: jest.fn(),
        send: jest.fn(),
    }
    const user = {
        id: 1,
        user: 'admin',
        role: 'admin',
    }
    const token = '1asdfewsdfwef'

    describe('login', () => {
        test('it should return token', async () => {
            sinon.stub(repository, 'findOne').resolves(user)
            sinon.stub(service, 'comparePassword').resolves(true)
            sinon.stub(service, 'getToken').resolves(token)

            await authHandler.login(req, res)
            expect(res.send).toHaveBeenCalledWith({token})
        })

        test('it should return http status code 403 if password is invalid', async () => {
            sinon.stub(repository, 'findOne').resolves(user)
            sinon.stub(service, 'comparePassword').resolves(false)

            await authHandler.login(req, res)
            expect(res.sendStatus).toHaveBeenCalledWith(403)
        })

        test('it should return http status code 500 if user is undefined', async () => {
            sinon.stub(repository, 'findOne').rejects('user is undefined')

            await authHandler.login(req, res)
            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })
})