import {createPool} from 'mysql2/promise'

const pool = createPool({
    host: process.env.MYSQL_HOST || 'localhost',
    user: process.env.MYSQL_USER || 'root',
    password: process.env.MYSQL_PASS || 'admin',
    database: process.env.MYSQL_DB || 'test',
})

export const getPool = () => pool.getConnection()
