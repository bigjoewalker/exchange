
export const getInsertStringQuery = (model, tableName) => {
    let tables = ''

    Object.keys(model).forEach((key, index, array) => {
        if (index === array.length - 1) {
            tables += `${key}`
        } else {
            tables += `${key},`
        }
    })

    return `insert into ${tableName} (${tables}) values ?`
}

export const getInsertValues = (models) => {
    return [
        models.map(item => {
            return Object.keys(item).map(key => {
                return item[key]
            })
        })
    ]
}