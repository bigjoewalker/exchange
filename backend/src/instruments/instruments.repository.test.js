import * as repository from './instruments.repository'

const pool = {
    query: jest.fn(),
    release: jest.fn(),
}

afterEach(() => {
    jest.clearAllMocks()
})

describe('instruments.repository', () => {
    describe('find', () => {
        test('it should return all instruments', async () => {
            pool.query.mockResolvedValue([[{}, {}]])
            const instruments = await repository.find(pool)

            expect(pool.release).toHaveBeenCalled()
            expect(instruments.length).toEqual(2)
        })
    })

    describe('findById', () => {
        test('it should return instrument id equal 2', async () => {
            pool.query.mockResolvedValue([[{InstrumentId: 2}]])
            const {InstrumentId} = await repository.findById(pool, 2)

            expect(pool.release).toHaveBeenCalled()
            expect(InstrumentId).toEqual(2)
        })
    })

    describe('insertOne', () => {
        test('it should return insert status successful', async () => {
            pool.query.mockResolvedValue([])
            const instrument = {
                OMSId: 1,
                InstrumentId: 16,
                Symbol: 'QUSDTTHB',
                Product1: 7,
                Product1Symbol: 'USDT',
                Product2: 3,
                Product2Symbol: 'THB',
                InstrumentType: 'Standard',
                VenueInstrumentId: 16,
                VenueId: 1,
                SortIndex: 0,
                SessionStatus: 'Running',
                PreviousSessionStatus: 'Running',
                SessionStatusDateTime: '2021-02-03T22:30:15.427Z',
                SelfTradePrevention: true,
                QuantityIncrement: 1e-8,
                PriceIncrement: 1e-8,
                MinimumQuantity: 1e-8,
                MinimumPrice: 1e-8,
                VenueSymbol: 'QUSDTTHB',
                IsDisable: false,
                MasterDataId: 0,
                PriceCollarThreshold: 0,
                PriceCollarPercent: 0,
                PriceCollarEnabled: false,
                PriceFloorLimit: 0,
                PriceFloorLimitEnabled: false,
                PriceCeilingLimit: 0,
                PriceCeilingLimitEnabled: false,
                CreateWithMarketRunning: true,
                AllowOnlyMarketMakerCounterParty: false,
                PriceCollarIndexDifference: 0,
                PriceCollarConvertToOtcEnabled: false,
                PriceCollarConvertToOtcClientUserId: 0,
                PriceCollarConvertToOtcAccountId: 0,
                PriceCollarConvertToOtcThreshold: 0,
                OtcConvertSizeThreshold: 0,
                OtcConvertSizeEnabled: false,
                OtcTradesPublic: false,
                PriceTier: 0
            }
            const status = await repository.insertOne(pool, instrument)

            expect(pool.release).toHaveBeenCalled()
            expect(status).toEqual('successful')
        })
    })

    describe('insertMany', () => {
        test('it should return insert status successful', async () => {
            pool.query.mockResolvedValue([])
            const instruments = [
                {
                    OMSId: 1,
                    InstrumentId: 22,
                    Symbol: 'XRPBTC',
                    Product1: 6,
                    Product1Symbol: 'XRP',
                    Product2: 1,
                    Product2Symbol: 'BTC',
                    InstrumentType: 'Standard',
                    VenueInstrumentId: 22,
                    VenueId: 1,
                    SortIndex: 0,
                    SessionStatus: 'Running',
                    PreviousSessionStatus: 'Unknown',
                    SessionStatusDateTime: '2021-02-28T13:01:59.926Z',
                    SelfTradePrevention: true,
                    QuantityIncrement: 1,
                    PriceIncrement: 1e-8,
                    MinimumQuantity: 8,
                    MinimumPrice: 1e-8,
                    VenueSymbol: 'XRPBTC',
                    IsDisable: false,
                    MasterDataId: 0,
                    PriceCollarThreshold: 0,
                    PriceCollarPercent: 0,
                    PriceCollarEnabled: false,
                    PriceFloorLimit: 0,
                    PriceFloorLimitEnabled: false,
                    PriceCeilingLimit: 0,
                    PriceCeilingLimitEnabled: false,
                    CreateWithMarketRunning: true,
                    AllowOnlyMarketMakerCounterParty: false,
                    PriceCollarIndexDifference: 0,
                    PriceCollarConvertToOtcEnabled: false,
                    PriceCollarConvertToOtcClientUserId: 0,
                    PriceCollarConvertToOtcAccountId: 0,
                    PriceCollarConvertToOtcThreshold: 0,
                    OtcConvertSizeThreshold: 0,
                    OtcConvertSizeEnabled: false,
                    OtcTradesPublic: false,
                    PriceTier: 0
                },
                {
                    OMSId: 1,
                    InstrumentId: 23,
                    Symbol: 'XLMBTC',
                    Product1: 5,
                    Product1Symbol: 'XLM',
                    Product2: 1,
                    Product2Symbol: 'BTC',
                    InstrumentType: 'Standard',
                    VenueInstrumentId: 23,
                    VenueId: 1,
                    SortIndex: 0,
                    SessionStatus: 'Running',
                    PreviousSessionStatus: 'Unknown',
                    SessionStatusDateTime: '2021-02-27T13:42:21.918Z',
                    SelfTradePrevention: true,
                    QuantityIncrement: 1,
                    PriceIncrement: 1e-8,
                    MinimumQuantity: 10,
                    MinimumPrice: 1e-8,
                    VenueSymbol: 'XLMBTC',
                    IsDisable: false,
                    MasterDataId: 0,
                    PriceCollarThreshold: 0,
                    PriceCollarPercent: 0,
                    PriceCollarEnabled: false,
                    PriceFloorLimit: 0,
                    PriceFloorLimitEnabled: false,
                    PriceCeilingLimit: 0,
                    PriceCeilingLimitEnabled: false,
                    CreateWithMarketRunning: true,
                    AllowOnlyMarketMakerCounterParty: false,
                    PriceCollarIndexDifference: 0,
                    PriceCollarConvertToOtcEnabled: false,
                    PriceCollarConvertToOtcClientUserId: 0,
                    PriceCollarConvertToOtcAccountId: 0,
                    PriceCollarConvertToOtcThreshold: 0,
                    OtcConvertSizeThreshold: 0,
                    OtcConvertSizeEnabled: false,
                    OtcTradesPublic: false,
                    PriceTier: 0
                }
            ]
            const status = await repository.insertMany(pool, instruments)

            expect(pool.release).toHaveBeenCalled()
            expect(status).toEqual('successful')
        })
    })

    describe('remove', () => {
        test('it should return status remove successful', async () => {
            pool.query.mockResolvedValue([])
            const status = await repository.remove(pool, 1)

            expect(pool.release).toHaveBeenCalled()
            expect(status).toEqual('successful')
        })
    })
})