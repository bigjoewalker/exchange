import {getInsertStringQuery, getInsertValues} from '../database/database.query.helper'

const tableName = 'instrument'

export const find = async (pool) => {
    const [rows] = await pool.query(`select * from ${tableName}`)

    pool.release()
    return rows
}

export const findById = async (pool, id) => {
    const [rows] = await pool.query(`select * from ${tableName} where InstrumentId=${id}`)

    pool.release()
    return rows[0]
}


export const insertOne = async (pool, instrument) => {
    const values = getInsertValues([instrument])
    const query = getInsertStringQuery(instrument, tableName)
    await pool.query(query, values)

    pool.release()
    return 'successful'
}

export const insertMany = async (pool, instruments) => {
    const values = getInsertValues(instruments)
    const query = getInsertStringQuery(instruments[0], tableName)
    await pool.query(query, values)

    pool.release()
    return 'successful'
}

export const remove = async (pool, id) => {
    await pool.query(`delete from ${tableName} where InstrumentId=${id}`)

    pool.release()
    return 'successful'
}
