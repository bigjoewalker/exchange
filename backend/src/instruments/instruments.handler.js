import * as repository from './instruments.repository'
import * as db from '../database/database.connection'

export const getAll = async (req, res) => {
    try {
        const pool = await db.getPool()
        const instruments = await repository.find(pool)
        return res.send(instruments)
    } catch (e) {
        console.log(`get all instruments fail: ${e}`)
        return res.sendStatus(500)
    }
}

export const getById = async (req, res) => {
    try {
        const id = req.params.id
        const pool = await db.getPool()
        const instrument = await repository.findById(pool, id)
        return res.send(instrument)
    } catch (e) {
        console.log(`find instrument id ${req.params.id} fail: ${e}`)
        return res.sendStatus(500)
    }
}

export const save = async (req, res) => {
    try {
        const payload = req.body
        const pool = await db.getPool()
        if (payload.length > 0) {
            await repository.insertMany(pool, payload)
        } else {
            await repository.insertOne(pool, payload)
        }

        return res.sendStatus(200)
    } catch (e) {
        console.log(`save instrument fail: ${e}`)
        return res.sendStatus(500)
    }
}

export const remove = async (req, res) => {
    try {
        const id = req.params.id
        const pool = await db.getPool()
        await repository.remove(pool, id)
        return res.sendStatus(200)
    } catch (e) {
        console.log(`remove instrument id ${req.params.id} fail: ${e}`)
        return res.sendStatus(500)
    }
}
