import sinon from 'sinon'
import * as handler from './instruments.handler'
import * as repository from './instruments.repository'

beforeAll(() => {
    jest.spyOn(console, 'log').mockImplementation(() => {
    })
})

afterEach(() => {
    jest.clearAllMocks()
    sinon.restore()
})

describe('instrument.handler', () => {
    const req = {
        body: {},
        params: {
            id: 22
        }
    }
    const res = {
        locals: {},
        sendStatus: jest.fn(),
        send: jest.fn(),
    }
    const instruments = [
        {
            OMSId: 1,
            InstrumentId: 22,
            Symbol: 'XRPBTC',
            Product1: 6,
            Product1Symbol: 'XRP',
            Product2: 1,
            Product2Symbol: 'BTC',
            InstrumentType: 'Standard',
            VenueInstrumentId: 22,
            VenueId: 1,
            SortIndex: 0,
            SessionStatus: 'Running',
            PreviousSessionStatus: 'Unknown',
            SessionStatusDateTime: '2021-02-28T13:01:59.926Z',
            SelfTradePrevention: true,
            QuantityIncrement: 1,
            PriceIncrement: 1e-8,
            MinimumQuantity: 8,
            MinimumPrice: 1e-8,
            VenueSymbol: 'XRPBTC',
            IsDisable: false,
            MasterDataId: 0,
            PriceCollarThreshold: 0,
            PriceCollarPercent: 0,
            PriceCollarEnabled: false,
            PriceFloorLimit: 0,
            PriceFloorLimitEnabled: false,
            PriceCeilingLimit: 0,
            PriceCeilingLimitEnabled: false,
            CreateWithMarketRunning: true,
            AllowOnlyMarketMakerCounterParty: false,
            PriceCollarIndexDifference: 0,
            PriceCollarConvertToOtcEnabled: false,
            PriceCollarConvertToOtcClientUserId: 0,
            PriceCollarConvertToOtcAccountId: 0,
            PriceCollarConvertToOtcThreshold: 0,
            OtcConvertSizeThreshold: 0,
            OtcConvertSizeEnabled: false,
            OtcTradesPublic: false,
            PriceTier: 0
        },
        {
            OMSId: 1,
            InstrumentId: 23,
            Symbol: 'XLMBTC',
            Product1: 5,
            Product1Symbol: 'XLM',
            Product2: 1,
            Product2Symbol: 'BTC',
            InstrumentType: 'Standard',
            VenueInstrumentId: 23,
            VenueId: 1,
            SortIndex: 0,
            SessionStatus: 'Running',
            PreviousSessionStatus: 'Unknown',
            SessionStatusDateTime: '2021-02-27T13:42:21.918Z',
            SelfTradePrevention: true,
            QuantityIncrement: 1,
            PriceIncrement: 1e-8,
            MinimumQuantity: 10,
            MinimumPrice: 1e-8,
            VenueSymbol: 'XLMBTC',
            IsDisable: false,
            MasterDataId: 0,
            PriceCollarThreshold: 0,
            PriceCollarPercent: 0,
            PriceCollarEnabled: false,
            PriceFloorLimit: 0,
            PriceFloorLimitEnabled: false,
            PriceCeilingLimit: 0,
            PriceCeilingLimitEnabled: false,
            CreateWithMarketRunning: true,
            AllowOnlyMarketMakerCounterParty: false,
            PriceCollarIndexDifference: 0,
            PriceCollarConvertToOtcEnabled: false,
            PriceCollarConvertToOtcClientUserId: 0,
            PriceCollarConvertToOtcAccountId: 0,
            PriceCollarConvertToOtcThreshold: 0,
            OtcConvertSizeThreshold: 0,
            OtcConvertSizeEnabled: false,
            OtcTradesPublic: false,
            PriceTier: 0
        }
    ]
    describe('getAll', () => {
        test('it should return all products', async () => {
            sinon.stub(repository, 'find').resolves(instruments)
            await handler.getAll(req, res)

            expect(res.send).toHaveBeenCalledWith(instruments)
        })

        test('it should return 500 if db connection error', async () => {
            sinon.stub(repository, 'find').rejects(new Error('error database connection'))
            await handler.getAll(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })

    describe('getById', () => {
        test('it should return instrument id 22', async () => {
            res.send.mockReturnValue(instruments[0])
            sinon.stub(repository, 'findById').resolves(instruments[0])
            const {InstrumentId} = await handler.getById(req, res)

            expect(InstrumentId).toEqual(22)
            expect(res.send).toHaveBeenCalledWith(instruments[0])
        })

        test('it should return 500 if db connection error', async () => {
            sinon.stub(repository, 'findById').rejects(new Error('error database connection'))
            await handler.getById(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })

    describe('save', () => {
        test('it should return status save successful if body length > 0', async () => {
            req.body = instruments
            sinon.stub(repository, 'insertMany').resolves('successful')
            await handler.save(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(200)
        })

        test('it should return status save successful if body length < 1', async () => {
            req.body = {...instruments[0]}
            sinon.stub(repository, 'insertOne').resolves('successful')
            await handler.save(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(200)
        })

        test('it should return 500 if db connection error', async () => {
            req.body = {...instruments[0]}
            sinon.stub(repository, 'insertOne').rejects(new Error('error database connection'))
            await handler.save(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })

    describe('remove', () => {
        test('it should return status remove successful', async () => {
            sinon.stub(repository, 'remove').resolves('successful')
            await handler.remove(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(200)
        })

        test('it should return 500 if db connection error', async () => {
            req.body = {...instruments[0]}
            sinon.stub(repository, 'remove').rejects(new Error('error database connection'))
            await handler.remove(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })
})