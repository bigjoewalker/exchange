import sinon from 'sinon'
import * as repository from './products.repository'
import * as handler from './products.handler'

beforeAll(() => {
    jest.spyOn(console, 'log').mockImplementation(() => {
    })
})

afterEach(() => {
    jest.clearAllMocks()
    sinon.restore()
})

describe('products.handler', () => {
    const req = {
        body: {},
        params: {
            id: 22
        }
    }
    const res = {
        locals: {},
        sendStatus: jest.fn(),
        send: jest.fn(),
    }
    
    const products = [
        {
            OMSId: 1,
            ProductId: 2,
            Product: 'ETH',
            ProductFullName: 'Ethereum',
            MasterDataUniqueProductSymbol: '',
            ProductType: 'CryptoCurrency',
            DecimalPlaces: 5,
            TickSize: 0.00001,
            NoFees: false,
            IsDisabled: false,
            MarginEnabled: false
        },
        {
            OMSId: 1,
            ProductId: 3,
            Product: 'THB',
            ProductFullName: 'Thai Baht',
            MasterDataUniqueProductSymbol: '',
            ProductType: 'NationalCurrency',
            DecimalPlaces: 2,
            TickSize: 0.01,
            NoFees: false,
            IsDisabled: false,
            MarginEnabled: false
        }
    ]
    describe('getAll', () => {
        test('it should return all products', async () => {
            sinon.stub(repository, 'find').resolves(products)
            await handler.getAll(req, res)

            expect(res.send).toHaveBeenCalledWith(products)
        })

        test('it should return 500 if db connection error', async () => {
            sinon.stub(repository, 'find').rejects(new Error('error database connection'))
            await handler.getAll(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })

    describe('getById', () => {
        test('it should return instrument id 2', async () => {
            res.send.mockReturnValue(products[0])
            sinon.stub(repository, 'findById').resolves(products[0])
            const {ProductId} = await handler.getById(req, res)

            expect(ProductId).toEqual(2)
            expect(res.send).toHaveBeenCalledWith(products[0])
        })

        test('it should return 500 if db connection error', async () => {
            sinon.stub(repository, 'findById').rejects(new Error('error database connection'))
            await handler.getById(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })

    describe('save', () => {
        test('it should return status save successful if body length > 0', async () => {
            req.body = products
            sinon.stub(repository, 'insertMany').resolves('successful')
            await handler.save(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(200)
        })

        test('it should return status save successful if body length < 1', async () => {
            req.body = {...products[0]}
            sinon.stub(repository, 'insertOne').resolves('successful')
            await handler.save(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(200)
        })

        test('it should return 500 if db connection error', async () => {
            req.body = {...products[0]}
            sinon.stub(repository, 'insertOne').rejects(new Error('error database connection'))
            await handler.save(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })

    describe('remove', () => {
        test('it should return status remove successful', async () => {
            sinon.stub(repository, 'remove').resolves('successful')
            await handler.remove(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(200)
        })

        test('it should return 500 if db connection error', async () => {
            req.body = {...products[0]}
            sinon.stub(repository, 'remove').rejects(new Error('error database connection'))
            await handler.remove(req, res)

            expect(res.sendStatus).toHaveBeenCalledWith(500)
        })
    })
})