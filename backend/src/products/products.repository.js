import {getInsertStringQuery, getInsertValues} from '../database/database.query.helper'

const tableName = 'product'

export const find = async (pool) => {
    const [rows] = await pool.query(`select * from ${tableName}`)

    pool.release()
    return rows
}

export const findById = async (pool, id) => {
    const [rows] = await pool.query(`select * from ${tableName} where ProductId=${id}`)

    pool.release()
    return rows[0]
}

export const insertOne = async (pool, product) => {
    const values = getInsertValues([product])
    const query = getInsertStringQuery(product, tableName)
    await pool.query(query, values)

    pool.release()
    return 'successful'
}

export const insertMany = async (pool, products) => {
    const values = getInsertValues(products)
    const query = getInsertStringQuery(products[0], tableName)
    await pool.query(query, values)

    pool.release()
    return 'successful'
}

export const remove = async (pool, id) => {
    await pool.query(`delete from product where ProductId=${id}`)

    pool.release()
    return 'successful'
}
