import * as repository from './products.repository'

const pool = {
    query: jest.fn(),
    release: jest.fn(),
}

afterEach(() => {
    jest.clearAllMocks()
})

describe('products.repository', () => {
    describe('find', () => {
        test('it should return all product', async () => {
            pool.query.mockResolvedValue([[{}, {}]])
            const products = await repository.find(pool)

            expect(pool.release).toHaveBeenCalled()
            expect(products.length).toEqual(2)
        })
    })

    describe('findById', () => {
        test('it should return product id equal 2', async () => {
            pool.query.mockResolvedValue([[{ProductId: 2}]])
            const {ProductId} = await repository.findById(pool, 2)

            expect(pool.release).toHaveBeenCalled()
            expect(ProductId).toEqual(2)
        })
    })

    describe('insertOne', () => {
        test('it should return insert status successful', async () => {
            pool.query.mockResolvedValue([])
            const product = {
                OMSId: 1,
                ProductId: 1,
                Product: 'BTC',
                ProductFullName: 'Bitcoin',
                MasterDataUniqueProductSymbol: '',
                ProductType: 'CryptoCurrency',
                DecimalPlaces: 8,
                TickSize: 1e-8,
                NoFees: false,
                IsDisabled: false,
                MarginEnabled: false
            }
            const status = await repository.insertOne(pool, product)

            expect(pool.release).toHaveBeenCalled()
            expect(status).toEqual('successful')
        })
    })

    describe('insertMany', () => {
        test('it should return insert status successful', async () => {
            pool.query.mockResolvedValue([])
            const products = [
                {
                    OMSId: 1,
                    ProductId: 2,
                    Product: 'ETH',
                    ProductFullName: 'Ethereum',
                    MasterDataUniqueProductSymbol: '',
                    ProductType: 'CryptoCurrency',
                    DecimalPlaces: 5,
                    TickSize: 0.00001,
                    NoFees: false,
                    IsDisabled: false,
                    MarginEnabled: false
                },
                {
                    OMSId: 1,
                    ProductId: 3,
                    Product: 'THB',
                    ProductFullName: 'Thai Baht',
                    MasterDataUniqueProductSymbol: '',
                    ProductType: 'NationalCurrency',
                    DecimalPlaces: 2,
                    TickSize: 0.01,
                    NoFees: false,
                    IsDisabled: false,
                    MarginEnabled: false
                }
            ]
            const status = await repository.insertMany(pool, products)

            expect(pool.release).toHaveBeenCalled()
            expect(status).toEqual('successful')
        })
    })

    describe('remove', () => {
        test('it should return status remove successful', async () => {
            pool.query.mockResolvedValue([])
            const status = await repository.remove(pool, 1)

            expect(pool.release).toHaveBeenCalled()
            expect(status).toEqual('successful')
        })
    })
})