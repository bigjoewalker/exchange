import * as repository from './products.repository'

export const getAll = async (req, res) => {
    try {
        const products = await repository.find()
        return res.send(products)
    } catch (e) {
        console.log(`get all products fail: ${e}`)
        return res.sendStatus(500)
    }
}

export const getById = async (req, res) => {
    try {
        const id = req.params.id
        const product = await repository.findById(id)
        return res.send(product)
    } catch (e) {
        console.log(`get product by id ${req.params.id} faile: ${e}`)
        return res.sendStatus(500)
    }
}

export const save = async (req, res) => {
    try {
        const payload = req.body
        if (payload.length > 0) {
            await repository.insertMany(payload)
        } else {
            await repository.insertOne(payload)
        }

        return res.sendStatus(200)
    } catch (e) {
        console.log(`save products fail: ${e}`)
        return res.sendStatus(500)
    }
}

export const remove = async (req, res) => {
    try {
        const id = req.params.id
        await repository.remove(id)
        return res.sendStatus(200)
    } catch (e) {
        console.log(`remove product id ${req.params.id} fail: ${e}`)
        return res.sendStatus(500)
    }
}