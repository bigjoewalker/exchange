import * as repository from './user.repository'

afterEach(() => {
    jest.clearAllMocks()
})

const pool = {
    query: jest.fn(),
    release: jest.fn(),
}

describe('user.repository', () => {
    describe('fineOne', () => {
        test('it should return admin user', async () => {
            const mockUser = {id: 1, user: 'admin', role: 'admin'}
            pool.query.mockResolvedValue([[mockUser]])
            const {id, user, role} = await repository.findOne(pool, {user: 'admin'})
            expect(id).toEqual(1)
            expect(user).toEqual('admin')
            expect(role).toEqual('admin')
        })
    })
})