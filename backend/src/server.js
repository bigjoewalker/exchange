import express from 'express'
import * as authHandler from './auth/auth.handler'
import * as authMiddleware from './auth/auth.middleware'
import * as instrumentsHandler from './instruments/instruments.handler'
import * as productsHander from './products/products.handler'

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.route('/v1/login')
    .post(authHandler.login)
app.use(authMiddleware.authValidateToken)
    .route('/v1/instruments')
    .get(instrumentsHandler.getAll)
    .post(instrumentsHandler.save)
app.use(authMiddleware.authValidateToken)
    .route('/v1/instruments/:id')
    .get(instrumentsHandler.getById)
    .delete(instrumentsHandler.remove)
app.use(authMiddleware.authValidateToken)
    .route('/v1/products')
    .get(productsHander.getAll)
    .post(productsHander.save)
app.use(authMiddleware.authValidateToken)
    .route('/v1/products/:id')
    .get(productsHander.getById)
    .delete(productsHander.remove)


const server = app.listen(3000, () => console.log('start listing port: 3000'))

process.on('SIGTERM', () => {
    console.log('SIGTERM signal received.')

    server.close(() => {
        console.log('Http server closed.')
        process.exit()
    })
})

// For test
export default app
